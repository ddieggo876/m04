<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Image acces</title>
            </head>
            <body>
               <style>
                  body {
                     display: flex;
                     justify-content: center;
                     align-items: center;
                     height: 100vh;
                     margin: 0;
                  }
                  img{
                    width: 100px;
                  }
                  table{
                     width: 100%;
                     border-collapse: collapse;
                  }
                  thead {
                     background-color: midnightblue;
                     color: white;
                  }
                  tr:nth-child(even){
                     background-color: whitesmoke;
                  }
                  th{
                     width: 200px;
                     padding-left: 20px;
                     padding-right: 20px;
                     padding-top: 15px;
                     padding-bottom: 15px;
                     text-align: center;
                  }
                  div{
                     overflow: hidden;
                     border-radius: 5px;
                  }
               
               </style>
               <div>
                  <table>
                     <thead class="titles">
                        <tr>
                           <th>Logo</th>
                           <th>Name</th>
                           <th>Type</th>
                           <th>License</th>
                        </tr>
                     </thead>
                     <tbody>
                        <xsl:for-each select="//program">
                            <tr>
                                <xsl:variable name="logo" select="logo" />
                                <td><img src="{$logo}" /></td>
                                <td><b><xsl:value-of select="name" /></b></td>
                                <td><xsl:value-of select="type" /></td>
                                <td><xsl:value-of select="license" /></td>
                            </tr>
                        </xsl:for-each>
                     </tbody>
                  </table>
               </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>