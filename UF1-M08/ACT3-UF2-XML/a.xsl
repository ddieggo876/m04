﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="text" indent="no"/>
    <xsl:template match="/" >
        Title:  <xsl:value-of select="//title" />
    </xsl:template>
</xsl:stylesheet>