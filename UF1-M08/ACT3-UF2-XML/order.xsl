﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Activitat 2</title>
            </head>
            <body>
                <style>
                    .rango1 {
                    background-color: yellow;
                    }
                    .rango2 {
                    background-color: green;
                    }
                    .rango3 {
                    background-color: red;
                    }
                </style>
                <h1>Products with a Price > 25 and Price &lt;= 100</h1>
                <div>
                    <xsl:apply-templates select="//destination"/>
                    <xsl:apply-templates select="order/products"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="destination">
        <table>
            <thead>
                <th>CUSTOMER DETAILS</th>
            </thead>
            <tbody>
                <tr>
                    <td>Name</td>
                    <td><xsl:value-of select="name" /></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><xsl:value-of select="address" /></td>
                </tr>
                <tr>
                    <td>City</td>
                    <td><xsl:value-of select="city" /></td>
                </tr>
                <tr>
                    <td>P.C</td>
                    <td><xsl:value-of select="postalcode" /></td>
                </tr>
            </tbody>
        </table>
    </xsl:template>
    <xsl:template match="products">
        <table>
            <thead><th>ORDER</th></thead>
            <tbody>
                <tr>
                    <td>Product</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Total</td>
                </tr>
                <xsl:for-each select="//product">
                <xsl:sort select="name" />
                <xsl:if test="price > 25 and price &lt;= 100">
                    <xsl:choose>
                    <xsl:when test="price &lt; 50">
                    <tr>
                        <td class="rango1"><xsl:value-of select="name" /> (code = <xsl:value-of select="@code" />)</td>
                        <td class="rango1"><xsl:value-of select="price" /></td>
                        <td class="rango1"><xsl:value-of select="quantity" /></td>
                        <td class="rango1">
                            <xsl:variable name="quantity" select="quantity" />
                            <xsl:variable name="price" select="price" />
                            <xsl:value-of select="$quantity * $price" />
                        </td>
                    </tr>
                    </xsl:when>
                    <xsl:when test="price &lt; 75">
                    <tr>
                        <td class="rango2"><xsl:value-of select="name" /> (code = <xsl:value-of select="@code" />)</td>
                        <td class="rango2"><xsl:value-of select="price" /></td>
                        <td class="rango2"><xsl:value-of select="quantity" /></td>
                        <td class="rango2">
                            <xsl:variable name="quantity" select="quantity" />
                            <xsl:variable name="price" select="price" />
                            <xsl:value-of select="$quantity * $price" />
                        </td>
                    </tr>
                    </xsl:when>
                    <xsl:when test="price &lt;= 100">
                    <tr>
                        <td class="rango3"><xsl:value-of select="name" /> (code = <xsl:value-of select="@code" />)</td>
                        <td class="rango3"><xsl:value-of select="price" /></td>
                        <td class="rango3"><xsl:value-of select="quantity" /></td>
                        <td class="rango3">
                            <xsl:variable name="quantity" select="quantity" />
                            <xsl:variable name="price" select="price" />
                            <xsl:value-of select="$quantity * $price" />
                        </td>
                    </tr>
                    </xsl:when>
                    </xsl:choose>
                </xsl:if>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>
</xsl:stylesheet>