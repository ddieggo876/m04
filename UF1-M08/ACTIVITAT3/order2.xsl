<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="order2.css" />
      </head>
      <body>
        <h2>Products with a Price &gt; 25 and Price &lt;= 100</h2>
        
        <table>
          <tr style="background-color: blue; color: white;">
            <th colspan="4" class="head1">ORDER</th>
          </tr>
          <tr class="head2">
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
          </tr>
          <!-- Products generated using generateRow -->
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Gray Curtains'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Green Paint 25L blanca'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Metal Chair'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Side Table'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'White Paint 30L'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Wood Chair'"/>
          </xsl:call-template>
          <xsl:call-template name="generateRow">
            <xsl:with-param name="productName" select="'Wood Table'"/>
          </xsl:call-template>
          <!-- Products with price between 25 and 100 -->
          <xsl:apply-templates select="/order/products/product[price &gt; 25 and price &lt;= 100]" />
        </table>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template name="generateRow">
    <xsl:param name="productName"/>
    <xsl:variable name="product" select="/order/products/product[name = $productName]" />
    <xsl:variable name="total" select="$product/quantity * $product/price"/>
    <tr>
      <xsl:choose>
        <xsl:when test="$product/price &gt; 25 and $product/price &lt; 50">
          <xsl:attribute name="class">yellow</xsl:attribute>
        </xsl:when>
        <xsl:when test="$product/price &gt;= 50 and $product/price &lt; 75">
          <xsl:attribute name="class">green</xsl:attribute>
        </xsl:when>
        <xsl:when test="$product/price &gt;= 75 and $product/price &lt;= 100">
          <xsl:attribute name="class">red</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class">normal</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <td>
        <xsl:value-of select="concat($product/name, ' (code = ', $product/@code, ')')"/>
      </td>
      <td><xsl:value-of select="$product/price"/></td>
      <td><xsl:value-of select="$product/quantity"/></td>
      <td><xsl:value-of select="$total"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
