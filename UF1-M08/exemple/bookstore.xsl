<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h2>Bookstore - Conditional</h2>
                <xsl:for-each select="bookstore/book">
                <ul>
                    <li>Title: <xs:value-of select="title" /></li>
                    <li>Author: <xs:value-of select="author" /></li>
                    <xsl:if test="price>=35">
                      <li style="color: red">Price: <xsl:value-of select="price"/></li>
                    </xsl:if>
                    <xsl:if test="price &lt; 35">
                      <li style="color: green">Price: <xsl:value-of select="price"/></li>
                    </xsl:if>
                </ul>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>