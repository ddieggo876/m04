<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>XSL Transform</title>
            <meta charset="UTF-8" />
            <style>
                th {
                    background-color: #1e395b; /* Azul oscuro */
                    color: white; /* Texto blanco */
                }
                
                td:first-child {
                    font-weight: bold; /* Texto en negrita para la primera columna (Title) */
                }
    
                tr:nth-child(odd) {
                    background-color: #f2f2f2; /* Fondo gris casi blanco para filas pares */
                }
            </style>
        </head>
        <body>
            <h2>Film list</h2>
            <table border="1">
                <tr bgcolor="#9acd32">
                    <th>Title</th>
                    <th>Year</th>
                    <th>Country</th>
                    <th>Genere</th>
                    <th>Summary</th>
                </tr>
                <xsl:for-each select="//film">
                    <tr>
                        <td>
                            <pr><xsl:value-of select="title" /></pr>
                        </td>
                        <td>
                            <xsl:value-of select="year" />
                        </td>
                        <td>
                            <xsl:value-of select="country" />
                        </td>
                        <td>
                            <xsl:value-of select="genre" />
                        </td>
                        <td>
                            <xsl:value-of select="summary" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    
    </xsl:template>
</xsl:stylesheet>