<html>
    <head>
      <link href="stylesheet/css" />
    </head>
    <body>
      <table>
        <thead>
          <th>TITLE</th>
          <th>EDITORIAL</th>
          <th>PRICE</th>
        </thead>
        <tbody>
          {
            for $book in /bookstore/book
            return
              <tr>
                <td>{data($book/title)}</td>
                <td>{data($book/editorial)}</td>
                <td>{data($book/price)}</td>
              </tr>
          }
        </tbody>
      </table>
    </body>
</html>