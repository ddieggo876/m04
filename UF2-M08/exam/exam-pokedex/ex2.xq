let $doc := doc("pokedex.xml")
return
<pokemons>
{
  for $i in /pokedex/pokemon
  order by $i/species
  return
    <pokemon>
      <name>{data($i/species)}</name>
      <abilities>
        {$i/abilities/ability}
        {$i/abilities/dream}
      </abilities>
    </pokemon>
}
</pokemons>