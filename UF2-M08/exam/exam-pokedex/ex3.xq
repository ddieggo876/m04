let $doc := doc("pokedex.xml")
return
<pokemons>{
for $i in $doc/pokedex/pokemon
let $suma := $i/baseStats/HP + $i/baseStats/ATK + $i/baseStats/DEF + $i/baseStats/SPD + $i/baseStats/SDEF + $i/baseStats/SATK
order by $suma descending
return
  <pokemon>
    {$i/species}
    <total>{$suma}</total>
  </pokemon>}
</pokemons>