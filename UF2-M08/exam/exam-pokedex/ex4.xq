let $doc := doc("pokedex.xml")
return
<pokemons>
  {
    for $i in $doc/pokedex/pokemon
    where $i/types/type="FLYING"
    order by $i/species
    return
    <pokemon>
      {$i/species}
      {$i/types}
    </pokemon>
  }
</pokemons>