let $doc := doc("pokedex.xml")
return
<pokemons>
{
  for $i in /pokedex/pokemon
  where $i/baseStats/ATK>100
  return
    <pokemon>
      {$i/species}
      {$i/baseStats/ATK}
    </pokemon>
}
</pokemons>