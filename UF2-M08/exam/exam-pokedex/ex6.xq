let $doc := doc("pokedex.xml")
return 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Activitat 2 coding</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <h1>Pokemons</h1>
    <div>
      <table>
        <thead>
          <th>Image</th>
          <th>Pokemon</th>
          <th>HP</th>
          <th>ATK</th>
          <th>DEF</th>
          <th>SPD</th>
          <th>SATK</th>
          <th>SDEF</th>
        </thead>
        <tbody>
        {
          for $i in $doc/pokedex/pokemon
          return
          <tr>
            <td><img src="{data($i/image)}"/></td>
            <td>{data($i/species)}</td>
            <td>{data($i/baseStats/HP)}</td>
            <td>{data($i/baseStats/ATK)}</td>
            <td>{data($i/baseStats/DEF)}</td>
            <td>{data($i/baseStats/SPD)}</td>
            <td>{data($i/baseStats/SATK)}</td>
            <td>{data($i/baseStats/SDEF)}</td>
          </tr>
        }
        </tbody>
      </table>
    </div>
  </body>
</html>